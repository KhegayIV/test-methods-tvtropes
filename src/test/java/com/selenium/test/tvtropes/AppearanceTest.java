package com.selenium.test.tvtropes;

import com.codeborne.selenide.Condition;
import com.selenium.test.tvtropes.pages.ContentPage;
import org.junit.Test;
import org.openqa.selenium.WebElement;

import static com.codeborne.selenide.Condition.cssClass;
import static com.codeborne.selenide.Selenide.open;

public class AppearanceTest extends TestBase {

    @Test
    public void nightVisionTest() {
        final ContentPage home = openHome();
        whileLogged(() -> {
            home.getBody().shouldHave(color(239, 239, 239));
            home.toggleNight();
            home.getBody().shouldHave(color(102, 102, 102));
            home.toggleNight();
            home.getBody().shouldHave(color(239, 239, 239));
        });
    }

    @Test
    public void spoilerTest() {
        ContentPage page = open("http://tvtropes.org/pmwiki/pmwiki.php/Main/SpoilerHound", ContentPage.class);
        page.getSpoilerHoundSpoilerElement().shouldHave(cssClass("spoiler"));
        page.toggleSpoiler();
        page.getSpoilerHoundSpoilerElement().shouldHave(cssClass("spoiler-off"));
        page.toggleSpoiler();
        page.getSpoilerHoundSpoilerElement().shouldHave(cssClass("spoiler"));
    }

    private static ColorCondition color(int r, int g, int b) {
        return new ColorCondition("rgba(" + r + ", " + g + ", " + b + ", 1)");
    }

    private static class ColorCondition extends Condition {

        private String color;

        public ColorCondition(String color) {
            super("element color");
            this.color = color;
        }

        @Override
        public boolean apply(WebElement webElement) {
            return webElement.getCssValue("background-color").equalsIgnoreCase(color);
        }

        @Override
        public String actualValue(WebElement element) {
            return element.getCssValue("background-color");
        }
    }
}
