package com.selenium.test.tvtropes;

import com.codeborne.selenide.Condition;
import com.selenium.test.tvtropes.pages.ContentPage;
import org.junit.Test;
import org.openqa.selenium.WebElement;

import static com.codeborne.selenide.Condition.text;

public class ContentTest extends TestBase {


    @Test
    public void randomTropeTest() {
        final ContentPage home = openHome();
        final ContentPage tropePage = home.clickRandomTrope();
        tropePage.getPageTitle().shouldNotBe(new IsMediaTitle());
    }

    @Test
    public void randomMediaTest() {
        final ContentPage home = openHome();
        final ContentPage mediaPage = home.clickRandomMedia();
        mediaPage.getPageTitle().shouldBe(new IsMediaTitle());
        String media = mediaPage.getMedia();
        mediaPage.getRandomMediaTooltip().shouldHave(text("Random " + media));
        mediaPage.rerollMediaPage().getPageTitle().shouldHave(new MediaTypeTitle(media));
    }

    private static class IsMediaTitle extends Condition {
        public IsMediaTitle() {
            super("media page check");
        }

        @Override
        public String actualValue(WebElement element) {
            return element.getText().contains("/") ? "media page" : "trope page";
        }

        @Override
        public boolean apply(WebElement webElement) {
            return webElement.getText().contains("/");
        }
    }

    private static class MediaTypeTitle extends Condition {
        private String mediaType;

        public MediaTypeTitle(String mediaType) {
            super("media type check");
            this.mediaType = mediaType;
        }

        @Override
        public String actualValue(WebElement element) {
            return element.getText().contains("/") ? element.getText().split("/")[0] : "null";
        }

        @Override
        public boolean apply(WebElement webElement) {
            return webElement.getText().contains("/") && webElement.getText().split("/")[0].startsWith(mediaType);
        }
    }
}
