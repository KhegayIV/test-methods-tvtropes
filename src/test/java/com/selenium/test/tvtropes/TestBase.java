package com.selenium.test.tvtropes;

import com.selenium.test.tvtropes.pages.ContentPage;
import com.selenium.test.tvtropes.pages.TVTropesLogonPage;
import com.selenium.test.tvtropes.pages.TVTropesStartPage;
import org.apache.log4j.Logger;
import org.junit.Before;

import static com.codeborne.selenide.Selenide.open;

public abstract class TestBase {
    public static final String DRIVER_LOCATION = "D:\\WebDriver\\chromedriver.exe";
    protected Logger logger = Logger.getLogger(getClass());

    @Before
    public void setUp() throws Exception {
        System.setProperty("webdriver.chrome.driver", DRIVER_LOCATION);
        System.setProperty("selenide.browser", "chrome");
        System.setProperty("selenide.start-maximized", "false");
       /*ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        WebDriverRunner.setWebDriver(new ChromeDriver(options));*/
    }

    protected ContentPage openHome() {
        return open("http://tvtropes.org/", ContentPage.class);
    }

    protected void whileLogged(Runnable action) {
        TVTropesStartPage startPage = open("http://tvtropes.org/", TVTropesStartPage.class);
        TVTropesLogonPage logonPage = startPage.clickLogonIcon();
        logonPage.insertUserCredentials("solitarymite", "testPassword1994");
        logonPage.logon();
        logger.debug("Logged in");
        action.run();
        startPage.logOut();
        logger.debug("Logged out");
    }


}
