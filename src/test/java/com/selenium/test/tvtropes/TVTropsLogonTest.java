package com.selenium.test.tvtropes;

import com.selenium.test.tvtropes.pages.TVTropesLogonPage;
import com.selenium.test.tvtropes.pages.TVTropesRetrievePasswordPage;
import com.selenium.test.tvtropes.pages.TVTropesRetrieveUserNamePage;
import com.selenium.test.tvtropes.pages.TVTropesStartPage;
import com.selenium.test.utils.MailUtil;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Test;

import static com.codeborne.selenide.Selenide.open;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TVTropsLogonTest extends TestBase {
    private static final Logger LOG = LogManager.getLogger("TVTropsLogonTests");

    @Test
    public void testAuthorizationCorrect() {
        String startUrl = "http://tvtropes.org/";
        TVTropesStartPage startPage = open(startUrl, TVTropesStartPage.class);
        LOG.info(String.format("Open %s", startUrl));
        TVTropesLogonPage logonPage = startPage.clickLogonIcon();
        logonPage.insertUserCredentials("solitarymite", "testPassword1994");
        startPage = logonPage.logon();
        assertTrue(startPage.isAuthenticated());
        startPage.logOut();
    }

    @Test
    public void testAuthorizationWrongPasswordFail() {

        TVTropesStartPage startPage = open("http://tvtropes.org/", TVTropesStartPage.class);
        TVTropesLogonPage logonPage = startPage.clickLogonIcon();
        logonPage.insertUserCredentials("solitarymite", "aludfygbvla");
        startPage = logonPage.logon();
        assertFalse(logonPage.isAuthorizationCorrect());
    }

    @Test
    public void testAuthorizationRetrievePasswordInvalideEmailFormatFail() {

        TVTropesStartPage startPage = open("http://tvtropes.org/", TVTropesStartPage.class);
        TVTropesLogonPage logonPage = startPage.clickLogonIcon();
        logonPage.insertUserCredentials("solitarymite", "aludfygbvla");
        logonPage.logon();
        TVTropesRetrievePasswordPage retrievePasswordPage = logonPage.resetPassClick();
        retrievePasswordPage.insertUserCredentials("solitarymite", "voaytrefvbauerv");
        retrievePasswordPage.resetPasswordClick();
        assertFalse(retrievePasswordPage.isEmailValide());
    }

    @Test
    public void testAuthorizationRetrievePasswordWrongEmailFail() {

        TVTropesStartPage startPage = open("http://tvtropes.org/", TVTropesStartPage.class);
        TVTropesLogonPage logonPage = startPage.clickLogonIcon();
        logonPage.insertUserCredentials("solitarymite", "aludfygbvla");
        logonPage.logon();
        TVTropesRetrievePasswordPage retrievePasswordPage = logonPage.resetPassClick();
        retrievePasswordPage.insertUserCredentials("solitarymite", "voaytrefvbauerv@mail.ru");
        retrievePasswordPage.resetPasswordClick();
        assertFalse(retrievePasswordPage.isResetFormCorrect());
    }

    @Test
    public void testAuthorizationRetrieveUserNameWrongEmailFail() {

        TVTropesStartPage startPage = open("http://tvtropes.org/", TVTropesStartPage.class);
        TVTropesLogonPage logonPage = startPage.clickLogonIcon();
        TVTropesRetrieveUserNamePage retrieveUserNamePage = logonPage.retrieveUserNameClick();

        retrieveUserNamePage.insertUserCredentials("apyfbvlahbriu7grgbvf@mail.ru");
        retrieveUserNamePage.resetUserNameClick();
        assertTrue(retrieveUserNamePage.isWrongEmailErrorOccure());
    }

    @Test
    public void testAuthorizationRetrieveUserNameEpmtyEmailFail() {

        TVTropesStartPage startPage = open("http://tvtropes.org/", TVTropesStartPage.class);
        TVTropesLogonPage logonPage = startPage.clickLogonIcon();

        TVTropesRetrieveUserNamePage retrieveUserNamePage = logonPage.retrieveUserNameClick();

        retrieveUserNamePage.resetUserNameClick();
        assertTrue(retrieveUserNamePage.isEmptyEmailErrorOccure());
    }

    @Test
    public void testAuthorizationRetrievePasswordValideEmailCorrect() {

        TVTropesStartPage startPage = open("http://tvtropes.org/", TVTropesStartPage.class);
        TVTropesLogonPage logonPage = startPage.clickLogonIcon();
        logonPage.insertUserCredentials("solitarymite", "skjtgbuorbtguetxd");
        logonPage.logon();
        TVTropesRetrievePasswordPage retrievePasswordPage = logonPage.resetPassClick();
        retrievePasswordPage.insertUserCredentials("solitarymite", "stz_lab@mail.ru");
        retrievePasswordPage.resetPasswordClick();
        String newPassword = MailUtil.getNewPasswordFromEmail();
        logonPage = startPage.clickLogonIcon();
        logonPage.insertUserCredentials("solitarymite", newPassword);
        startPage = logonPage.logon();
        assertTrue(startPage.isAuthenticated());
        startPage.changePassword(newPassword, "testPassword1994");
        startPage.logOut();
    }
}
