package com.selenium.test.tvtropes;

import com.selenium.test.tvtropes.pages.ContentPage;
import com.selenium.test.tvtropes.pages.WatchPage;
import org.junit.Test;

import static com.codeborne.selenide.Condition.exist;

public class WatchTest extends TestBase {

    @Test
    public void testWatch() {
        whileLogged(() -> {
            ContentPage home = openHome();
            home.clickWatchButton();
            WatchPage watchlist = home.watchlist();
            watchlist.getHomePageElement().should(exist);
            home = openHome();
            home.clickWatchButton();
            home.watchlist();
            watchlist = home.watchlist();
            watchlist.getHomePageElement().shouldNot(exist);
        });
    }
}
