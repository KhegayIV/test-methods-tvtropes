package com.selenium.test.tvtropes.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.selenium.test.utils.ScreenShotUtil;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.Selenide.page;

public class TVTropesRetrievePasswordPage {

    private Logger logger = Logger.getLogger(TVTropesRetrievePasswordPage.class);

    @FindBy(id = "modal-pass-reset-username")
    private SelenideElement userNameInputElement;

    @FindBy(id = "modal-pass-reset-email")
    private SelenideElement userEmailInputElement;

    @FindBy(id = "modal-pass-reset-submit")
    private SelenideElement submitButtonElement;

    @FindBy(css = "#modal-pass-reset-form fieldset")
    private SelenideElement invalidEmailErrorElement;

    @FindBy(css = "#modal-pass-reset-form div[class='server-response']")
    private SelenideElement serverResponce;

    public void insertUserCredentials(String userName, String userEmail) {
        logger.debug("Inputting login/email : " + userName + " / " + userEmail);
        ScreenShotUtil.takeScreenShot();
        userNameInputElement.sendKeys(userName);
        userEmailInputElement.sendKeys(userEmail);
        ScreenShotUtil.takeScreenShot();
    }

    public TVTropesStartPage resetPasswordClick() {
        logger.debug("Clicking reset password");
        ScreenShotUtil.takeScreenShot();
        submitButtonElement.click();
        try {
            serverResponce.waitWhile(Condition.not(Condition.exist), 5000);
        } catch (Exception e) {
        }

        return page(TVTropesStartPage.class);
    }

    public boolean isEmailValide() {
        findInvalidEmailErrorElement();
        return !invalidEmailErrorElement.exists();
    }

    public boolean isResetFormCorrect() {
        return !serverResponce.exists();
    }

    private void findInvalidEmailErrorElement() {
        invalidEmailErrorElement = invalidEmailErrorElement.find(By.cssSelector("div[class='form-group']")).find(By.xpath("//*[contains(text(), 'Please enter a valid email')]"));
    }
}
