package com.selenium.test.tvtropes.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.Selenide.page;

public class TVTropesChangePasswordPage {
    @FindBy(id = "change-pass")
    private SelenideElement changePasswordForm;

    @FindBy(css = "input[class='form-control cur-pass']")
    private SelenideElement currentPasswordInputElement;

    @FindBy(css = "input[class='form-control new-pass']")
    private SelenideElement newPasswordInputElement;

    private SelenideElement submitButtonElement;

    private void findSubmitButton() {
        submitButtonElement = changePasswordForm.find(By.xpath("//*[contains(text(), 'Update Password')]"));
    }

    public void insertUserCredentials(String curPassword, String newPassword) {
        currentPasswordInputElement.sendKeys(curPassword);
        newPasswordInputElement.sendKeys(newPassword);
    }

    public TVTropesUserPersonalPage changePasswordSubmit() {
        findSubmitButton();
        submitButtonElement.click();
        return page(TVTropesUserPersonalPage.class);
    }
}
