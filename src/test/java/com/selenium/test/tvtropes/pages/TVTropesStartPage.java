package com.selenium.test.tvtropes.pages;


import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.selenium.test.utils.ScreenShotUtil;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.Selenide.page;

public class TVTropesStartPage {
    private Logger logger = Logger.getLogger(TVTropesStartPage.class);
    @FindBy(css = "a[class='LoginModalLink user-img']")
    private SelenideElement logonIconElement;

    @FindBy(css = "a[class='LoginModalLink user-img is-loggedIn']")
    private SelenideElement logoutIconElement;

    @FindBy(css = "a[href='/pmwiki/unknower.php?handle=solitarymite']")
    private SelenideElement logoutListLinkElement;

    @FindBy(css = "a[href='/pmwiki/awl.php']")
    private SelenideElement watchlistListLinkElement;
    @FindBy(css = "a[href='/pmwiki/profile.php']")
    private SelenideElement profileListLinkElement;

    public TVTropesLogonPage clickLogonIcon() {

        logger.debug("Click logon");
        ScreenShotUtil.takeScreenShot();
        logonIconElement.click();
        ScreenShotUtil.takeScreenShot();
        return page(TVTropesLogonPage.class);
    }

    public TVTropesStartPage logOut() {
        logoutIconElement.click();
        logger.debug("Click logout");
        ScreenShotUtil.takeScreenShot();
        logoutListLinkElement.click();
        ScreenShotUtil.takeScreenShot();
        return page(TVTropesStartPage.class);
    }


    public WatchPage watchlist() {
        logger.debug("Open watchlist");
        ScreenShotUtil.takeScreenShot();
        logonIconElement.click();
        ScreenShotUtil.takeScreenShot();
        watchlistListLinkElement.click();
        ScreenShotUtil.takeScreenShot();
        return page(WatchPage.class);
    }

    public TVTropesUserPersonalPage navigateToProfile() {
        logger.debug("Click user icon");
        ScreenShotUtil.takeScreenShot();
        logoutIconElement.click();
        ScreenShotUtil.takeScreenShot();
        profileListLinkElement.click();
        logger.debug("Click 'profile' list element");
        ScreenShotUtil.takeScreenShot();
        return page(TVTropesUserPersonalPage.class);
    }

    public boolean isAuthenticated() {
        try {
            logoutIconElement.waitUntil(Condition.exist, 5000);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void changePassword(String curPassword, String newPassword) {
        TVTropesUserPersonalPage userPersonalPage = navigateToProfile();
        TVTropesChangePasswordPage changePasswordPage = userPersonalPage.changePasswordClick();
        logger.debug("Click change password");
        ScreenShotUtil.takeScreenShot();
        changePasswordPage.insertUserCredentials(curPassword, newPassword);
        ScreenShotUtil.takeScreenShot();
        changePasswordPage.changePasswordSubmit();
        ScreenShotUtil.takeScreenShot();
    }
}
