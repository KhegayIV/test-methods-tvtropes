package com.selenium.test.tvtropes.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.Selenide.page;

public class TVTropesUserPersonalPage {
    @FindBy(css = "div [class='item pull-right extra-options hidden-xs']")
    private SelenideElement extraOptions;


    @FindBy(css = "a[data-target='#change-password-modal']")
    private SelenideElement changePasswordElement;

    public TVTropesChangePasswordPage changePasswordClick() {
        changePasswordElement.click();
        return page(TVTropesChangePasswordPage.class);
    }
}
