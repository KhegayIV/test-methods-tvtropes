package com.selenium.test.tvtropes.pages;


import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.selenium.test.utils.ScreenShotUtil;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.Selenide.page;

public class TVTropesLogonPage {

    private Logger logger = Logger.getLogger(TVTropesLogonPage.class);

    @FindBy(id = "modal-login-username")
    private SelenideElement userNameInputElement;

    @FindBy(id = "modal-login-password")
    private SelenideElement userPassInputElement;

    @FindBy(id = "modal-login-submit")
    private SelenideElement submitButtonElement;

    @FindBy(css = "#modal-login-form")
    private SelenideElement modelLoginForm;

    @FindBy(css = "#modal-login-form div[class='server-response']")
    private SelenideElement serverResponce;

    @FindBy(css = "#modal-login-form div[class='server-response']")
    private SelenideElement resetPassLinkElement;

    @FindBy(css = "li[class='get-handle-tab']")
    private SelenideElement retrieveUserNameLinkElement;

    public void insertUserCredentials(String userName, String userPass) {
        logger.debug("Inputting login/pass : " + userName + "/" + userPass);
        ScreenShotUtil.takeScreenShot();
        userNameInputElement.sendKeys(userName);
        userPassInputElement.sendKeys(userPass);
        ScreenShotUtil.takeScreenShot();
    }

    public TVTropesStartPage logon() {
        logger.debug("Clicking sign in");
        ScreenShotUtil.takeScreenShot();
        submitButtonElement.click();
        ScreenShotUtil.takeScreenShot();
        try {
            serverResponce.waitWhile(Condition.not(Condition.exist), 5000);
        } catch (Exception e) {
        }
        return page(TVTropesStartPage.class);
    }

    public boolean isAuthorizationCorrect() {
        return !serverResponce.exists();
    }

    public TVTropesRetrievePasswordPage resetPassClick() {
        logger.debug("Clicking reset pass");
        ScreenShotUtil.takeScreenShot();
        resetPassLinkElement = serverResponce.find(By.cssSelector("a"));
        resetPassLinkElement.click();
        ScreenShotUtil.takeScreenShot();
        return page(TVTropesRetrievePasswordPage.class);
    }

    public TVTropesRetrieveUserNamePage retrieveUserNameClick() {
        logger.debug("Clicking retrieve username");
        ScreenShotUtil.takeScreenShot();
        retrieveUserNameLinkElement.click();
        return page(TVTropesRetrieveUserNamePage.class);
    }
}
