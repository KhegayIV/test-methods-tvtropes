package com.selenium.test.tvtropes.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.selenium.test.utils.ScreenShotUtil;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.FindBy;

public class TVTropesRetrieveUserNamePage {

    private Logger logger = Logger.getLogger(TVTropesRetrieveUserNamePage.class);

    @FindBy(id = "modal-get-handle-email")
    private SelenideElement userEmailInputElement;

    @FindBy(id = "modal-get-handle-submit")
    private SelenideElement submitButtonElement;

    @FindBy(css = "#modal-get-handle-form div[class='server-response']")
    private SelenideElement serverResponceElement;

    public void insertUserCredentials(String userEmail) {
        userEmailInputElement.sendKeys(userEmail);
        logger.debug("Trying to retrieve login for " + userEmail);
        ScreenShotUtil.takeScreenShot();
    }

    public void resetUserNameClick() {
        submitButtonElement.click();
    }

    public boolean isFormCorrect() {
        try {
            serverResponceElement.waitWhile(Condition.hasText(""), 5000);
        } catch (Exception e) {
        }
        return !serverResponceElement.exists();
    }

    public boolean isEmptyEmailErrorOccure() {
        try {
            serverResponceElement.waitUntil(Condition.hasText("Email cannot be empty."), 5000);
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    public boolean isWrongEmailErrorOccure() {
        try {
            serverResponceElement.waitUntil(Condition.hasText("We don't have that email address on file."), 5000);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
