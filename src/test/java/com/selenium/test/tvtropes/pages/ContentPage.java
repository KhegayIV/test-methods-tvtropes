package com.selenium.test.tvtropes.pages;

import com.codeborne.selenide.SelenideElement;
import com.selenium.test.utils.ScreenShotUtil;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

public class ContentPage extends TVTropesStartPage {
    private Logger logger = Logger.getLogger(ContentPage.class);

    @FindBy(id = "body")
    private SelenideElement body;

    @FindBy(css = "a[class$='left-menu-btn']")
    private SelenideElement leftMenuBtn;

    @FindBy(css = "a[class='btn-close']")
    private SelenideElement closeBtn;

    @FindBy(css = "a[class='toggle-random-btns']")
    private SelenideElement randomBtns;

    @FindBy(css = "h1[class='page-title']")
    private SelenideElement pageTitle;

    @FindBy(css = "li[class^='display-btn nightvision-btn']")
    private SelenideElement nightVisionToggle;

    @FindBy(css = "li[class^='display-btn spoiler-btn']")
    private SelenideElement spoilerToggle;

    @FindBy(css = "a[class^='watch-page']")
    private SelenideElement watchButton;

    @FindBy(xpath = "//a[contains(text(), 'Random Tropes')]")
    private SelenideElement tropeButton;

    @FindBy(xpath = "//a[contains(text(), 'Random Media')]")
    private SelenideElement mediaButton;

    @FindBy(css = "a[class = 'rand-body-btn']")
    private SelenideElement randomMediaButton;

    @FindBy(css = "div[class = 'random-btn-tooltip']")
    private SelenideElement randomMediaTooltip;

    public ContentPage clickWatchButton() {
        watchButton.click();
        return this;
    }

    public ContentPage clickRandomMedia() {
        logger.debug("Random media");
        if (randomBtns.isDisplayed()) {
            ScreenShotUtil.takeScreenShot();
            randomBtns.click();
        }
        ScreenShotUtil.takeScreenShot();
        mediaButton.click();
        ScreenShotUtil.takeScreenShot();
        return page(ContentPage.class);
    }

    public ContentPage clickRandomTrope() {
        logger.debug("Random trope");
        if (randomBtns.isDisplayed()) {
            ScreenShotUtil.takeScreenShot();
            randomBtns.click();
        }
        ScreenShotUtil.takeScreenShot();
        tropeButton.click();
        ScreenShotUtil.takeScreenShot();
        return page(ContentPage.class);
    }

    public ContentPage toggleSpoiler() {
        logger.debug("Toggle spoiler");
        ScreenShotUtil.takeScreenShot();
        leftMenuBtn.click();
        ScreenShotUtil.takeScreenShot();
        spoilerToggle.click();
        ScreenShotUtil.takeScreenShot();
        closeBtn.click();
        ScreenShotUtil.takeScreenShot();
        return this;
    }

    public ContentPage toggleNight() {
        logger.debug("Toggle night");
        ScreenShotUtil.takeScreenShot();
        leftMenuBtn.click();
        ScreenShotUtil.takeScreenShot();
        nightVisionToggle.click();
        ScreenShotUtil.takeScreenShot();
        closeBtn.click();
        ScreenShotUtil.takeScreenShot();
        return this;
    }

    public String getMedia() {
        String text = pageTitle.text();
        if (!text.contains("/")) {
            return null;
        } else {
            return text.split("/")[0];
        }
    }

    public boolean isMedia() {
        return pageTitle.text().contains("/");
    }

    public SelenideElement getSpoilerElement(String text) {
        return $(By.xpath("//span[contains(text(), '" + text + "')]"));
    }

    public SelenideElement getSpoilerHoundSpoilerElement() {
        return getSpoilerElement("annoyed");
    }

    public SelenideElement getBody() {
        return body;
    }

    public SelenideElement getPageTitle() {
        return pageTitle;
    }

    public ContentPage rerollMediaPage() {
        ScreenShotUtil.takeScreenShot();
        logger.debug("Reroll media");
        randomMediaButton.click();
        return page(ContentPage.class);
    }

    public SelenideElement getRandomMediaTooltip() {
        return randomMediaTooltip;
    }
}
